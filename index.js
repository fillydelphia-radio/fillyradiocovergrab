const express = require("express");
const bodyParser = require("body-parser");
const { getCoverFromFiles } = require("./functions/getCoverFromFiles");

const app = express();

app.use(bodyParser.json());

// expects object in body:
/**
 * {
 *  filePath: 'path/to/file',
 *  basePath: `path/to/redact`,  // root of the media folder
 *  defaultImage: 'https://link.to/image/file',
 *  isLive: false, // bool
 *  metadata: [object Object] // should contain title and artist
 * }
 */
app.post("/getcover", async (req, res, next) => {
  const { filePath, basePath, defaultImage, isLive, metadata } = req.body;
  console.log(isLive);
  let pathToFile = "";
  if (!isLive) {
    pathToFile = process.env.MEDIAROOT + "/" + filePath.replace(basePath, "");
  }
  const response = await getCoverFromFiles(
    pathToFile,
    defaultImage,
    isLive,
    metadata
  );
  res.json(response);
});

app.post("/findcover", async (req, res, next) => {});

app.listen(3000);
