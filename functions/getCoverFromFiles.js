const fs = require("fs");
const mm = require("music-metadata");
const axios = require("axios");
const albumArt = require("album-art");
const { processImages } = require("./processImages");

async function getCoverFromFiles(path, defaultImage, isLive, metadata) {
  console.log(path);
  let image;
  if (isLive) {
    console.log(metadata.liveCover);
    let imgUrl;
    if (metadata.liveCover && metadata.liveCover.includes("http")) {
      imgUrl = await axios({
        url: metadata.liveCover,
        responseType: "arraybuffer",
      });
      image = imgUrl.data;
    } else {
      imgUrl = await axios({
        url: defaultImage,
        responseType: "arraybuffer",
      });
      image = imgUrl.data;
    }
  } else {
    if (fs.existsSync(path)) {
      try {
        const metadata = await mm.parseFile(path);
        image = metadata.common.picture[0].data;
      } catch (e) {
        console.log(e);
        let defaultImg = await axios({
          url: defaultImage,
          responseType: "arraybuffer",
        });
        image = defaultImg.data;
      }
    } else {
      let defaultImg = await axios({
        url: defaultImage,
        responseType: "arraybuffer",
      });
      image = defaultImg.data;
    }
  }
  const imageObject = await processImages(image);
  return imageObject;
}

module.exports.getCoverFromFiles = getCoverFromFiles;
