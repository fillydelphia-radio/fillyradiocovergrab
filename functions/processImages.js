const md5 = require("md5");
const sharp = require("sharp");
const fs = require("fs");

function processImages(image) {
  const hash = md5(image);
  console.log(hash);
  const exists = fs.existsSync("./images/" + hash + ".jpg");
  if (exists) {
    return {
      hash,
    };
  } else {
    return Promise.all([
      sharp(image)
        .resize(96, 96)
        .jpeg({ quality: 70, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-96.jpg"),
      sharp(image)
        .resize(128, 128)
        .jpeg({ quality: 70, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-128.jpg"),
      sharp(image)
        .resize(192, 192)
        .jpeg({ quality: 70, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-192.jpg"),
      sharp(image)
        .resize(256, 256)
        .jpeg({ quality: 80, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-256.jpg"),
      sharp(image)
        .resize(384, 384)
        .jpeg({ quality: 80, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-384.jpg"),
      sharp(image)
        .resize(512, 512)
        .jpeg({ quality: 80, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + "-512.jpg"),
      sharp(image)
        .jpeg({ quality: 90, chromaSubsampling: "4:4:4" })
        .toFile("./images/" + hash + ".jpg"),
    ])
      .then(() => {
        return {
          hash,
        };
      })
      .catch((e) => {
        console.log(e);
        return false;
      });
  }
}

module.exports.processImages = processImages;
